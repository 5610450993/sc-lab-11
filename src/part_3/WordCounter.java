package part_3;
import java.util.Arrays;
import java.util.HashMap;


public class WordCounter {
	private String message;
	private HashMap<String, Integer> wordCount = new HashMap<String, Integer>();
	
	public WordCounter(String message){
		this.message = message;
	}
	
	public void count(){
		String[] m = message.split(" ");
		for(int i = 0 ; i < m.length ; i++){
			wordCount.put(m[i], 0);
		}
		for(int i = 0 ; i < m.length ; i++){
			wordCount.put(m[i], wordCount.get(m[i])+1);
		}
	}
	
	public int hasWord(String word){
		if(wordCount.get(word) == null) return 0;
		else return wordCount.get(word);
	}
	
	public static void main(String[] args) {
		WordCounter c = new WordCounter("here is the root of the root and the bud of the bud");
		c.count();
		System.out.println(c.hasWord("root"));
		System.out.println(c.hasWord("leaf"));
	}
}
