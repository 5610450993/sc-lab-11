package part_5;
import java.util.ArrayList;


public class Refrigerator {
	private int size;
	private ArrayList<String> things = new ArrayList<String>();
	
	public Refrigerator(int size){
		this.size = size;
	}
	
	public void put(String stuff) throws FullException{
		if(things.size() >= size) throw new FullException("Error Full Fridge");
		else things.add(stuff);
	}
	
	public String takeOut(String stuff){
		for(int i = 0 ; i < things.size() ; i++){
			if(things.get(i).equals(stuff)){
				things.remove(i);
				return stuff;
			}
		}
		return null;
	}
	
	public String toString(){
		return things.toString();
	}
	
	public static void main(String[] args) {
		try{
			Refrigerator r = new Refrigerator(3);
			System.out.println("PUT: Coke, Water, Dessert");
			r.put("Coke");
			r.put("Water");
			r.put("Dessert");
			//Error "Remove Comments"
			//r.put("Vegetable");
			System.out.println("TAKEOUT: Coke,Pepsi");
			System.out.println("Coke: "+r.takeOut("Coke"));
			System.out.println("Pepsi: "+r.takeOut("Pepsi"));
			System.out.println("Total: "+r.toString());
		}
		catch(FullException e){
			System.out.println("Error Full Fridge");
		}

	}

}
